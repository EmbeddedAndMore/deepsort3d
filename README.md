
# DeepSort3D

DeepSort3D is 3D pedestrian tracker inspired from DeepSort. 3nd dimention is can be provided using LIDAR or Stereo camera (like Intel Realsense)

Depth estimation has been improved using keypoint extractor [AlphaPose](https://github.com/MVIG-SJTU/AlphaPose).\
Visual feature comparison has been provided using [Torchreid](https://github.com/KaiyangZhou/deep-person-reid)



## Installation

Developement is done using [CUDA 10.2](https://developer.nvidia.com/cuda-10.2-download-archive)  in [Ubuntu 20.04 LTS](https://ubuntu.com/download/desktop)

Use the [pipenv](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pipenv install
```


## Create full test data (dataset_final.json)

In order to do that first, we are going to make sure that we have the key points estimation. for doing that we first activate the env:

```bash
conda activate alphapose
```
after that, we use this command to create the Json file containing all the information about bounding boxes and keypoints. so int the root of alphapose we type:

```bash
python scripts/demo_inference.py --cfg configs/coco/resnet/256x192_res50_lr1e-3_1x.yaml --checkpoint pretrained_models/fast_res50_256x192.pth --indir /home/mohamad/Kitti_tracking/data_tracking_image_2/training/image_02/0017 --gpus -1 --save_img --detbatch 1 --posebatch 1

```
the result will be generated with the name of **alphapose-results.json** in `examples/res`
#### points
`--indir`: is the folder containing images on which we want to do the estimation\
`--gpus -1 `: enables the CPU inference. it needs about 6 gigs of VRAM to be able to run on GPU. mine is 4, so I used CPU\
`--save_img`: make it save predictions on this path: `examples/res/vis`\
`--detbatch 1` and `--posebatch 1 ` makes it use less ram\

the format of generated data is like this:

```python
[
{
"image_id": "000000.png", 
"category_id": 1, 
"keypoints": [x, y, confidence, ...], 
"score": 2.803387403488159, 
"box": [x, y, w, h], 
"idx": [[0.0], [0.0]]
},..
]
```
`image_id`: name of the processed image\
`category_id`: id of the category that we are detecting, human is 1\
`keypoints`: location and confidence of each keypoints, depends on the config file (*.yaml) the numbers of the keypoint can be set to one of the: 17, 26, 136. so if it is set to 17 the length of the array would be `3*17=51`\
`score`: score of the whole estimation\
`box`: bounding box coordinates

after that, we use this file to crop detected pedestrians and feed it to the feature extractor to generate the feature array, a 1x512 array of float numbers that encode the features of the detected person. for that we have to switch to another env which is `torchreid` env:

```bash
conda activate torchreid
```

 In the end, we generate a dataset file with following model:

```python
@dataclass
class ObjectModel:
    bbox : List[int]
    upper : List[int]
    features : List[float]

@dataclass
class ImageModel:
    img_name:str
    objects:List[ObjectModel]
```
which generates Json file like this:
```
[
{
    img_name: ""
    objetcs:[
        {
          bbox:[1x4]
          upper: [1x4]
          features: [1x512]
        }
    ]
}
]
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
