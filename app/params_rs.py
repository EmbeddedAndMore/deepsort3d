tracker_version = "v2"
max_age = 10
n_init = 2
max_feature_distance = 0.2
max_mix_distance = 0.8
mix_coef = 0.4
max_distance = 0.8
NNBudget = 8
max_depth = 1000
feature_extractor = "osnet_ain_x1_0_dukemtmcreid"  # [osnet_x1_0_imagenet, osnet_ain_x1_0_market1501, osnet_ain_x1_0_msmt17, osnet_ain_x1_0_dukemtmcreid]
depth_projectio_area = "upper_kp"  # [keypoint, upper_bbox, bbox]
distance_measure_appraoch = "euclidean"  # [euclidean, mahalanobis]
detector = "yolor"  # [yolov4, yolov5m, yolov5x6, yolor]
colorblind_safe = [
    (255, 194, 10),
    (12, 123, 220),
    (153, 79, 0),
    (0, 108, 209),
    (225, 190, 106),
    (64, 176, 166),
    (230, 97, 0),
    (93, 58, 155),
    (26, 255, 26),
    (75, 0, 146),
    (254, 254, 98),
    (211, 95, 183),
    (0, 90, 181),
    (220, 50, 32),
    (26, 133, 255),
    (212, 17, 89),
]
