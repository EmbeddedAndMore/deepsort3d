import argparse
import os
import main_rs
import main_lidar
from plot import PlotIpm
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore, QtWidgets


def parse_args():
    parser = argparse.ArgumentParser(description="3D Deep SORT")
    parser.add_argument("--exec", help="Set lidar or rs(realsense)", default="rs", required=True)
    parser.add_argument(
        "--tracker",
        help="Set tracker version 1 or 2. 1: distance for tentative and features for approved. "
        "2: mix of dist and feature for approved and distance for unapproved",
        default="v2",
        required=False,
    )
    parser.add_argument("--fps", help="Print fps", default="True", required=False)
    parser.add_argument("--vis", help="visualize", default="True", required=False)

    return parser.parse_args()


def main_loop(pg, plot_widget):
    _exec = "rs"
    if _exec == "lidar":
        from params import tracker_version

        main_lidar.run(tracker_version, pg, plot_widget)
    else:
        from params_rs import tracker_version

        main_rs.run(tracker_version, pg, plot_widget)


if __name__ == "__main__":
    # args = parse_args()

    # main_loop(None, None)
    app = pg.mkQApp()
    plot_widget = pg.PlotWidget()
    pen = pg.mkPen("w")
    size = 10
    brush = pg.mkBrush("b")

    splitter = QtWidgets.QSplitter()
    splitter.addWidget(plot_widget)
    splitter.show()
    plot_widget.setXRange(-15, 15)
    plot_widget.setYRange(0, 100)

    timer = QtCore.QTimer()
    timer.timeout.connect(lambda: main_loop(pg, plot_widget))
    timer.start(0)

    pg.exec()
