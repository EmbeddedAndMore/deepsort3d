from abc import ABC, abstractmethod


class Detection:
    @abstractmethod
    def to_xyzah_ccs(self):
        pass

    @abstractmethod
    def to_xyz_ccs(self):
        pass

    @abstractmethod
    def to_tlbr_ccs(self):
        pass

    @abstractmethod
    def to_xyz_bottom_wcs(self):
        pass

    @abstractmethod
    def to_xyzah_wcs(self):
        pass

    @abstractmethod
    def to_xyz_wcs(self):
        pass

    @abstractmethod
    def to_size_wcs(self):
        pass
