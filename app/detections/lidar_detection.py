import numpy as np
from .base import Detection
from app.projections import Projection, LidarProjection


class LidarDetection(Detection):
    """
    This class represents a bounding box detection in a single image.

    Parameters
    ----------
    tlwh : array_like
        Bounding box in format `(x, y, w, h)`.
    confidence : float
        Detector confidence score.
    feature : array_like
        A feature vector that describes the object contained in this image.

    Attributes
    ----------
    tlwh : np.ndarray
        Bounding box in format `(top left x, top left y, width, height)` in pixel.
    confidence : np.ndarray
        Detector confidence score.
    feature : np.ndarray | NoneType
        A feature vector that describes the object contained in this image.

    """

    def __init__(self, tlwh, depth, confidence, feature, projector: LidarProjection):
        self.tlwh = np.asarray(tlwh, dtype=np.float)
        self.depth = depth
        self.confidence = float(confidence)
        self.feature = np.asarray(feature, dtype=np.float32)
        self.projector = projector

    def __str__(self):
        return f"depth: {self.depth} , tlwh: {self.tlwh}"

    def to_xyzah_ccs(self):
        """Convert bounding box to format `(center x (pixel), center y (pixel), depth (meter), aspect ratio (float),
        height (float))`, where the aspect ratio is `width / height`.
        """
        ret = np.zeros(shape=(5,))
        ret[:2] = self.tlwh[:2] + self.tlwh[2:] / 2
        ret[2] = self.depth
        ret[3] = self.tlwh[2] / self.tlwh[3]
        ret[4] = self.tlwh[3]
        return ret

    def to_xyz_ccs(self):
        xyzah_ccs = self.to_xyzah_ccs()
        return xyzah_ccs[:3]

    def to_tlbr_ccs(self):
        """in order to visualize bbounding box"""
        xyzah_ccs = self.to_xyzah_ccs()
        w = xyzah_ccs[3] * xyzah_ccs[4]
        h = xyzah_ccs[4]
        tlbr = np.array([int(xyzah_ccs[0] - w / 2), int(xyzah_ccs[1] - h / 2), int(w), int(h)])
        tlbr[2:4] += tlbr[0:2]
        return tlbr

    def to_xyz_bottom_wcs(self):
        xyzah_ccs = self.to_xyzah_ccs()
        xyzah_ccs[1] += xyzah_ccs[4] / 2
        xyz_wcs = self.projector.ccs2wcs(
            np.array([xyzah_ccs[0] * xyzah_ccs[2], xyzah_ccs[1] * xyzah_ccs[2], xyzah_ccs[2]])
        )

        return np.array([xyz_wcs[0], xyz_wcs[1], xyz_wcs[2], xyzah_ccs[3], xyzah_ccs[4]])

    def to_xyzah_wcs(self):
        xyzah_ccs = self.to_xyzah_ccs()
        xyz_wcs = self.projector.ccs2wcs(
            np.array([xyzah_ccs[0] * xyzah_ccs[2], xyzah_ccs[1] * xyzah_ccs[2], xyzah_ccs[2]])
        )

        return np.array([xyz_wcs[0], xyz_wcs[1], xyz_wcs[2], xyzah_ccs[3], xyzah_ccs[4]])

    def to_xyz_wcs(self):
        xyzah_ccs = self.to_xyzah_ccs()
        xyz_wcs = self.projector.ccs2wcs(
            np.array([xyzah_ccs[0] * xyzah_ccs[2], xyzah_ccs[1] * xyzah_ccs[2], xyzah_ccs[2]])
        )
        return xyz_wcs

    def to_size_wcs(self):
        tlbr_ccs = self.to_tlbr_ccs()
        tl_wcs = self.projector.ccs2wcs(np.array([tlbr_ccs[0] * self.depth, tlbr_ccs[1] * self.depth, self.depth]))

        br_wcs = self.projector.ccs2wcs(np.array([tlbr_ccs[2] * self.depth, tlbr_ccs[3] * self.depth, self.depth]))

        height = abs(tl_wcs[1]) + abs(br_wcs[1])
        width = 0.667334
        length = 0.902490
        return np.array([height, width, length])
