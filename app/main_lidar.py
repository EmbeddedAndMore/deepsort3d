import random
import time

import cv2
import numpy as np
from depth_perception import get_depth
import json
import sys
import os
from app.detections import LidarDetection
from app.projections import LidarProjection
from tracker_v1 import TrackerV1
from tracker_v2 import TrackerV2
from tracker_v3 import TrackerV3
from visualization.utils import draw_bbox_id
import nn_matching
from params import (
    depth_projectio_area,
    feature_extractor,
    max_depth,
    max_age,
    max_distance,
    max_mix_distance,
    max_feature_distance,
    mix_coef,
    n_init,
    NNBudget,
)
import params
from plot import PlotIpm
import threading


def read_calib_file(filepath):
    """
    Read in a calibration file and parse into a dictionary.
    Ref: https://github.com/utiasSTARS/pykitti/blob/master/pykitti/utils.py
    """
    data = {}
    with open(filepath, "r") as f:
        for line in f.readlines():
            line = line.rstrip()
            if len(line) == 0:
                continue
            key, value = line.split(":", 1)
            # The only non-float values in these files are dates, which
            # we don't care about anyway
            try:
                data[key] = np.array([float(x) for x in value.split()])
            except ValueError:
                pass

    return data


datasets = [
    "dataset_keypoints",
    "datasets_upper_body_keypoints",
    "datasets_upper_body_rect",
    "dataset_yolov5m_upper_body",
    "dataset_yolov5m_bbox",
    "dataset_yolov5x6_upper_bbox_osnet_x1_0_imagenet",
    "dataset_yolov5x6_bbox_osnet_x1_0_imagenet",
    "dataset_yolov5x6_upper_bbox_osnet_ain_x1_0_msmt17",
    "dataset_yolov5x6_bbox_osnet_ain_x1_0_msmt17",
    "dataset_yolov5x6_upper_bbox_osnet_ain_x1_0_market1501",
    "dataset_yolov5x6_bbox_osnet_ain_x1_0_market1501",
    "dataset_yolov5x6_upper_bbox_osnet_ain_x1_0_dukemtmcreid",
    "dataset_yolov5x6_bbox_osnet_ain_x1_0_dukemtmcreid",
]


def prepare_dataset_files(dataset_name):
    # load dataset file
    with open(
        f"../notebooks/dataset_yolov5x6_{depth_projectio_area}_{feature_extractor}/dataset_final_train_00{dataset_name}.json",
        "r",
    ) as f:
        content = f.read()
    dataset = json.loads(content)
    frames = f"/home/eam/KittiDs/data_tracking_image_2/training/image_02/00{dataset_name}"
    tracklets_1 = np.load("../res/tracklet_indices.npy")
    tracklets_2 = np.load("../res/tracklet_indices1.npy")
    tracklet_indices = [tracklets_1, tracklets_2]
    min_length = sys.maxsize
    for item in tracklet_indices:
        min_length = min(min_length, len(item))
    # tracklet_indices = np.array([tracklets_1[:min_length], tracklets_2[:min_length]])
    tracklet_indices = np.zeros(shape=(1, len(dataset)))
    calib = read_calib_file(f"/home/eam/KittiDs/data_tracking_calib/training/calib/00{dataset_name}.txt")
    return dataset, frames, tracklet_indices, calib


def run(tracker_version: str, pg, plot_widget):
    do_eval = False

    # threading.Thread(target=plt_ipm.start(), args=(1,)).start()
    for epoch in range(0, 21):
        dataset_name = f"{epoch:02d}"
        dataset, frames, tracklet_indices, calib = prepare_dataset_files(dataset_name)

        projector = LidarProjection(calib)

        # init tracker
        metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_feature_distance, NNBudget)
        if tracker_version == "v1":
            tracker = TrackerV1(metric, max_distance=max_distance, max_age=max_age, n_init=n_init, projector=projector)
        elif tracker_version == "v2":
            tracker = TrackerV2(
                metric,
                max_distance=max_mix_distance,
                max_age=max_age,
                n_init=n_init,
                projector=projector,
                mix_coef=mix_coef,
            )
        else:
            tracker = TrackerV3(
                metric,
                max_distance=max_mix_distance,
                max_age=max_age,
                n_init=n_init,
                projector=projector,
                mix_coef=mix_coef,
            )
        logs = []
        # loop through each frame's info (until where we have tracklet in scene)
        for i in range(tracklet_indices.shape[1]):
            print(f"loop: {i}")
            step = dataset[i]
            img_name = step["img_name"]
            objects = np.array(step["objects"])
            print("Image name:", img_name)
            print("Object indices:", tracklet_indices[:, i])
            tracked_objects = objects  # objects[tracklet_indices[:, i]]
            detections = []
            for obj in tracked_objects:
                depth = get_depth(np.array(obj["lidars"][2]), 1.1, 2)
                if depth:
                    features = np.array(obj["features"])
                    if depth <= max_depth:
                        # depth = 0
                        detections.append(LidarDetection(np.array(obj["bbox"]), depth, 100, features, projector))

            tracker.predict()

            tracker.update(detections)
            # ============================
            xyz_wcs = np.array(
                [[*track.detection.to_xyz_wcs(), track.track_id] for track in tracker.tracks if track.is_confirmed()]
            )
            if xyz_wcs.size != 0:
                xz_wcs = xyz_wcs[:, [0, 2, 3]]
                kwargs = {
                    "x": xz_wcs[:, 0],
                    "y": xz_wcs[:, 1],
                    "pen": pg.mkPen("w"),
                    "brush": pg.mkBrush("b"),
                    "size": 10,
                }
            else:
                kwargs = {"x": [], "y": [], "pen": pg.mkPen("w"), "brush": pg.mkBrush("b"), "size": 10}
            item = pg.ScatterPlotItem(**kwargs)
            item.opts["useCache"] = False

            plot_widget.clear()
            plot_widget.addItem(item)
            plot_widget.repaint()
            # ==================================
            for track in tracker.tracks:
                if track.is_confirmed():
                    bbox = track.detection.to_tlbr_ccs()
                    dimension = track.detection.to_size_wcs()
                    location = track.detection.to_xyz_bottom_wcs()
                    log = "{0} {1} {2} {3:.6f} {4} {5:.6f} {6:.6f} {7:.6f} {8:.6f} {9:.6f} {10:.6f} {11:.6f} {12:.6f} {13:.6f} {14:.6f} {15:.6f} {16:0.6f} {17:0.6f}".format(
                        i,
                        track.track_id,
                        "Pedestrian",
                        -1.000000,
                        -1,
                        -1 * random.uniform(0, 6.283),
                        bbox[0],
                        bbox[1],
                        bbox[2],
                        bbox[3],
                        dimension[0],
                        dimension[1],
                        dimension[2],
                        location[0],
                        location[1],
                        location[2],
                        -10.000000,
                        random.uniform(0.7, 1),
                    )
                    logs.append(log)

            print("_______________________________________")

            # visualization
            frame_path = os.path.join(frames, img_name)
            frame = cv2.imread(frame_path)
            for track in tracker.tracks:
                if track.is_confirmed():
                    bbox = track.detection.to_tlbr_ccs()
                    frame = draw_bbox_id(frame, bbox, track.track_id)
                    pred_bbox = track.prediction_tlbr_ccs()
                    frame = draw_bbox_id(frame, pred_bbox, track.track_id, color=(0, 0, 255))
            if do_eval:
                k = cv2.waitKey(1)
            else:
                cv2.imshow("tracker", frame)
                cv2.moveWindow("tracker", 20, 600)
                cv2.setWindowTitle("tracker", img_name)
                k = cv2.waitKey(50)
            if k == 27:  # close on ESC key
                break
            if k == 115:  # press 's'
                print("Image_saved")
                cv2.imwrite(f"/home/eam/MasterThesis/Thesis/Report/{img_name}", frame)

        cv2.destroyAllWindows()

        if do_eval:
            with open(f"eval_result/Track_00{dataset_name}.txt", "w") as file:
                file.write("\n".join(logs))

    if do_eval:
        parameters = {
            key: value for key, value in params.__dict__.items() if not (key.startswith("__") or key.startswith("_"))
        }
        for key, value in parameters.items():
            print(f"{key}: ", value)

        print("eval_file_name: ", f'{time.strftime("%Y-%m-%d-%H-%M-%S")}')

        with open(f"eval_result/info.txt", "w") as file:
            j = json.dumps(parameters, indent=4)
            file.write(j)
