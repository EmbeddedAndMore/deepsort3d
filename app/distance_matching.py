import numpy as np
from scipy.spatial.distance import mahalanobis
import linear_assignment
from tracks import Track
from detections import Detection
from typing import List, Optional
from params import distance_measure_appraoch


def euclidean_distance(actual, predictions):
    """calculated distance between an actual point and list of predictions

    Parameters
    ----------
    actual: 1x3 3d point in wcs
    predictions: nx3 3d points in wcs

    Returns
    -------
    np.ndarray 1xn distances
    """
    return np.linalg.norm(predictions - actual, axis=-1)


def mahalanobis_distance_1d(actual, prediction, inv_cov):
    """calculated distance between an actual point and list of predictions

    Parameters
    ----------
    actual: 1x3 3d point in wcs
    prediction: 1x3 3d points in wcs
    inv_cov: inverse covariance matrix from kalman filter

    Returns
    -------
    np.ndarray 1xn distances
    """
    return mahalanobis(prediction, actual, inv_cov)


def mahalanobis_distance_2d(actual: np.ndarray, predictions, inv_cov):
    """calculated distance between an actual point and list of predictions

    Parameters
    ----------
    actual: 1x3 3d point in wcs
    predictions: nx3 3d points in wcs
    inv_cov: inverse covariance matrix from kalman filter

    Returns
    -------
    np.ndarray 1xn distances
    """
    return [mahalanobis(point, actual, inv_cov) for point in predictions]


def distance_cost(
    tracks: List[Track],
    detections: List[Detection],
    track_indices: Optional[List[int]] = None,
    detection_indices: Optional[List[int]] = None,
):
    """A distance metric.

    Parameters
    ----------
    tracks : List[deep_sort.track.Track]
        A list of tracks.
    detections : List[deep_sort.detection.Detection]
        A list of detections.
    track_indices : Optional[List[int]]
        A list of indices to tracks that should be matched. Defaults to
        all `tracks`.
    detection_indices : Optional[List[int]]
        A list of indices to detections that should be matched. Defaults
        to all `detections`.

    Returns
    -------
    ndarray
        Returns a cost matrix of shape
        len(track_indices), len(detection_indices) where entry (i, j) is
        `distance(tracks[track_indices[i]], detections[detection_indices[j]])`.

    """
    if track_indices is None:
        track_indices = np.arange(len(tracks))
    if detection_indices is None:
        detection_indices = np.arange(len(detections))

    cost_matrix = np.zeros((len(track_indices), len(detection_indices)))
    for row, track_idx in enumerate(track_indices):
        if tracks[track_idx].time_since_update > 1:
            cost_matrix[row, :] = linear_assignment.INFTY_COST
            continue
        if distance_measure_appraoch == "euclidean":
            bbox = tracks[track_idx].prediction_xyz_wcs()
            candidates = np.asarray([detections[i].to_xyz_wcs() for i in detection_indices])
            cost_matrix[row, :] = euclidean_distance(bbox, candidates)
        else:
            bbox = tracks[track_idx].prediction_xyzah_wcs()
            inv_cov_mat = tracks[track_idx].ukf.SI
            candidates = np.asarray([detections[i].to_xyzah_wcs() for i in detection_indices])
            cost_matrix[row, :] = mahalanobis_distance_2d(bbox, candidates, inv_cov_mat)

    return cost_matrix
