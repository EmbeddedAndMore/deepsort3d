import time
import cv2
import numpy as np
import json
import pyrealsense2 as rs
from tracker_v1 import TrackerV1
from tracker_v2 import TrackerV2
from tracker_v3 import TrackerV3
from visualization.utils import draw_bbox_id
import nn_matching
from depth_perception import get_depth
from app.detections import RsDetection
from app.projections import RsProjection
import random
import os
from params_rs import (
    depth_projectio_area,
    feature_extractor,
    max_depth,
    max_age,
    max_distance,
    max_mix_distance,
    max_feature_distance,
    mix_coef,
    n_init,
    NNBudget,
    colorblind_safe,
)
import params_rs


def read_calib_file(filepath):
    with open(filepath, "r") as f:
        json_obj = json.loads(f.read())
        intrinsics = rs.intrinsics()
        intrinsics.coeffs = json_obj["coeffs"]
        intrinsics.fx = json_obj["fx"]
        intrinsics.fy = json_obj["fy"]
        intrinsics.height = json_obj["height"]
        intrinsics.width = json_obj["width"]
        if json_obj["model"] == "distortion.brown_conrady":
            intrinsics.model = rs.distortion.brown_conrady
        elif json_obj["model"] == "distortion.inverse_brown_conrady":
            intrinsics.model = rs.distortion.inverse_brown_conrady
        intrinsics.ppx = json_obj["ppx"]
        intrinsics.ppy = json_obj["ppy"]
    return intrinsics


def prepare_dataset_files():
    # load dataset file
    ds1 = f"../res/dataset_final_rs_1.json"
    ds2 = f"/home/eam/MasterThesis/Thesis/deep-sort3d/notebooks/dataset_realsense_yolov5x6_upper_bbox_osnet_ain_x1_0_dukemtmcreid/complete.json"
    ds3 = "/home/eam/MasterThesis/Thesis/deep-sort3d/notebooks/dataset_realsense_yolor_p6_upper_bbox_osnet_ain_x1_0_dukemtmcreid/dataset_final_realsense_0.json"
    ds4 = "/home/eam/MasterThesis/Thesis/deep-sort3d/notebooks/dataset_realsense_yolor_p6_upper_bbox_osnet_ain_x1_0_market1501/dataset_final_realsense_0.json"
    ds5 = "/home/eam/MasterThesis/Thesis/deep-sort3d/notebooks/dataset_realsense_yolor_p6_upper_bbox_osnet_ain_x1_0_msmt17/dataset_final_realsense_0.json"
    ds6 = "/home/eam/MasterThesis/Thesis/deep-sort3d/notebooks/dataset_realsense_yolor_p6_upper_bbox_osnet_x1_0_imagenet/dataset_final_realsense_0.json"
    ds7 = "/home/eam/MasterThesis/Thesis/deep-sort3d/notebooks/dataset_realsense_upper_keypoiny/dataset.json"
    with open(ds7, "r") as f:
        _content = f.read()
    _dataset = json.loads(_content)
    _dataset = _dataset
    _frames = f"/home/eam/MasterThesis/Thesis/deep-sort3d/notebooks/extracted_rs_6_person/frames"

    _tracklet_indices = np.zeros(shape=(1, len(_dataset)))
    _depth_calib = read_calib_file(
        "/home/eam/MasterThesis/Thesis/deep-sort3d/notebooks/extracted_rs_6_person/calib/depth_intrinsics.json"
    )
    _color_calib = read_calib_file(
        "/home/eam/MasterThesis/Thesis/deep-sort3d/notebooks/extracted_rs_6_person/calib/color_intrinsics.json"
    )
    return _dataset, _frames, _tracklet_indices, _depth_calib, _color_calib


def run(tracker_version: str, pg, plot_widget):
    do_eval = False
    dataset, frames, tracklet_indices, depth_calib, color_calib = prepare_dataset_files()

    projector = RsProjection(depth_calib, color_calib)

    # init tracker
    # max_cosine_distance = 0.2
    # nn_budget = 10
    metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_feature_distance, NNBudget)
    if tracker_version == "v1":
        tracker = TrackerV1(metric, max_distance=max_distance, max_age=max_age, n_init=n_init, projector=projector)
    elif tracker_version == "v2":
        tracker = TrackerV2(
            metric,
            max_distance=max_mix_distance,
            max_age=max_age,
            n_init=n_init,
            projector=projector,
            mix_coef=mix_coef,
        )
    else:
        tracker = TrackerV3(
            metric,
            max_distance=max_mix_distance,
            max_age=max_age,
            n_init=n_init,
            projector=projector,
            mix_coef=mix_coef,
        )

    logs = []
    fpss = []
    # loop through each frame's info (until where we have tracklet in scene)
    for i in range(tracklet_indices.shape[1]):
        start = time.time()
        print(f"loop: {i}")
        step = dataset[i]
        img_name = step["img_name"]
        objects = np.array(step["objects"])
        print("Image name:", img_name)
        print("Object indices:", tracklet_indices[:, i])
        tracked_objects = objects  # objects[tracklet_indices[:, i]]
        detections = []
        for obj in tracked_objects:
            depth = get_depth(np.array(obj["lidars"]).flatten(), 1.1, 2)
            # depth = 0.1
            if depth:
                features = np.array(obj["features"])
                if depth < max_depth:
                    detections.append(RsDetection(np.array(obj["bbox"]), depth, 100, features, projector))

        tracker.predict()

        tracker.update(detections)
        # ============================
        xyz_wcs = np.array(
            [[*track.detection.to_xyz_wcs(), track.track_id] for track in tracker.tracks if track.is_confirmed()]
        )
        if xyz_wcs.size != 0:
            xz_wcs = xyz_wcs[:, [0, 2, 3]]
            kwargs = {
                "x": xz_wcs[:, 0],
                "y": xz_wcs[:, 1],
                "pen": pg.mkPen("w"),
                "brush": (0, 255, 0),  # pg.mkBrush("b")
                "size": 10,
            }
        else:
            kwargs = {"x": [], "y": [], "pen": pg.mkPen("w"), "brush": pg.mkBrush("b"), "size": 10}
        item = pg.ScatterPlotItem(**kwargs)
        item.opts["useCache"] = False

        plot_widget.clear()
        plot_widget.addItem(item)
        plot_widget.repaint()
        # ==================================
        if do_eval and i > 82:
            for track in tracker.tracks:
                if track.is_confirmed():
                    bbox = track.detection.to_tlbr_ccs()
                    dimension = track.detection.to_size_wcs()
                    location = track.detection.to_xyz_bottom_wcs()
                    log = "{0} {1} {2} {3:.6f} {4} {5:.6f} {6:.6f} {7:.6f} {8:.6f} {9:.6f} {10:.6f} {11:.6f} {12:.6f} {13:.6f} {14:.6f} {15:.6f} {16:0.6f} {17:0.6f}".format(
                        i - 83,
                        track.track_id,
                        "Pedestrian",
                        -1.000000,
                        -1,
                        -1 * random.uniform(0, 6.283),
                        bbox[0],
                        bbox[1],
                        bbox[2],
                        bbox[3],
                        dimension[0],
                        dimension[1],
                        dimension[2],
                        location[0],
                        location[1],
                        location[2],
                        -10.000000,
                        random.uniform(0.7, 1),
                    )
                    logs.append(log)
                # print(log)
                # print(f"tracker id: {track.track_id}, loc: {track.prediction_xyz_wcs()}")
            #    print("Dimensions: ", track.detection.to_size_wcs())
            # print("pred point: ", track.prediction_xyz_wcs())
            # print("actual point: ", detections[0].to_xyz_wcs())
        print("_______________________________________")

        # visualization
        frame_path = os.path.join(frames, img_name)
        frame = cv2.imread(frame_path)
        for track in tracker.tracks:
            if track.is_confirmed():
                bbox = track.detection.to_tlbr_ccs()
                color = colorblind_safe[track.track_id % len(colorblind_safe)]
                frame = draw_bbox_id(frame, bbox, track.track_id, color)
        # cv2.imshow("tracker", frame)
        # cv2.setWindowTitle("tracker", img_name)

        duration = time.time() - start
        fps = 1 / duration
        fpss.append(fps)
        print(f"fps: {fps}")

        if do_eval:
            k = cv2.waitKey(1)
        else:
            cv2.imshow("tracker", frame)
            cv2.moveWindow("tracker", 20, 600)
            cv2.setWindowTitle("tracker", img_name)
            k = cv2.waitKey(1)

        if k == 27:  # close on ESC key
            break
        if k == 115:  # press 's'
            print("Image_saved")
            cv2.imwrite(f"/home/eam/MasterThesis/Thesis/Report/{img_name}", frame)

    cv2.destroyAllWindows()

    if fpss:
        print("Avg fps: ", np.array(fpss).mean())

    if do_eval:
        with open(f"eval_result_rs/Track_rs_1.txt", "w") as file:
            file.write("\n".join(logs))

        parameters = {
            key: value for key, value in params_rs.__dict__.items() if not (key.startswith("__") or key.startswith("_"))
        }
        for key, value in parameters.items():
            print(f"{key}: ", value)

        print("eval_file_name: ", f'{time.strftime("%Y-%m-%d-%H-%M-%S")}')

        with open(f"eval_result_rs/info.txt", "w") as file:
            j = json.dumps(parameters, indent=4)
            file.write(j)
