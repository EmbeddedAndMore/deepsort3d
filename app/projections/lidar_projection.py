import numpy as np
from .base import Projection
from typing import List


class LidarProjection(Projection):
    """Convert point from world coordinate system to camera cs or vise versa"""

    def __init__(self, calib):
        self.calib = calib

    def wcs2ccs(self, xyd: np.ndarray):
        """convert a point from word coordinate system to camera coordinate system

        designed to convert 3D point gathered from LIDAR + Camera for KITTI dataset

        Parameters
        ----------
        xyd: list of floats
            x: in meter
            y: in meter
            d: in meter
        calib: np.dfarray
            calibration parameters

        Returns
        -------
        xyd: list
            x,y in pixel
            d in meter
        """
        p2 = self.calib["P2"].reshape(3, 4)
        offs = p2[:, 3]

        res = p2[:, :3] @ np.array(xyd)
        final = res + offs
        return final / xyd[2]

    def ccs2wcs(self, xyd: np.ndarray):
        """convert a point from camera coordinate system to word coordinate system

        designed to convert 3D point gathered from LIDAR + Camera for KITTI dataset

        Parameters
        ----------
        xyd: list of floats
            x: in pixel
            y: in pixel
            d: depth in meter
        calib: np.dfarray
            calibration parameters

        Returns
        -------
        xyd in meter in world coordinate system
        """
        p2 = self.calib["P2"].reshape(3, 4)
        offs = p2[:, 3]
        arrXYD = np.array(xyd)
        final = arrXYD - offs
        return np.linalg.inv(p2[:, :3]) @ final
