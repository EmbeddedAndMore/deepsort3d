import numpy as np
import pyrealsense2 as rs
from typing import List
from .base import Projection


class RsProjection(Projection):
    def __init__(self, depth_intrinsics, color_intrinsics):
        self.depth_calib = depth_intrinsics
        self.color_calib = color_intrinsics

    def wcs2ccs(self, xyd: np.ndarray):
        """convert a point from word coordinate system to camera coordinate system(point to pixel)

        designed to convert 3D point gathered from LIDAR + Camera for KITTI dataset

        Parameters
        ----------
        xyd: list of floats
            x: in meter
            y: in meter
            d: in meter
        calib: np.dfarray
            calibration parameters

        Returns
        -------
        xyd: list
            x,y in pixel
            d in meter
        """
        pixel = np.zeros((3,))
        pixel[:2] = rs.rs2_project_point_to_pixel(self.depth_calib, xyd)
        return pixel

    def ccs2wcs(self, xyd: np.ndarray):
        """convert a point from camera coordinate system to word coordinate system (pixel to point)

        designed to convert 3D point gathered from LIDAR + Camera for KITTI dataset

        Parameters
        ----------
        xyd: list of floats
            x: in pixel
            y: in pixel
            d: depth in meter
        calib: np.dfarray
            calibration parameters

        Returns
        -------
        xyd in meter in world coordinate system
        """
        depth_point = rs.rs2_deproject_pixel_to_point(self.depth_calib, xyd[0:2], xyd[2])
        return depth_point
