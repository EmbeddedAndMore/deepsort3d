from typing import List
from abc import ABC, abstractmethod
import numpy as np


class Projection(ABC):
    @abstractmethod
    def wcs2ccs(self, xyd: np.ndarray):
        pass

    @abstractmethod
    def ccs2wcs(self, xyd: np.ndarray):
        pass
