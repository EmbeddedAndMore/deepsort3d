from __future__ import absolute_import
import numpy as np
from scipy.optimize import linear_sum_assignment as linear_assignment


INFTY_COST = 1e5


def min_cost_matching(distance_metric, max_distance, tracks, detections, track_indices=None, detection_indices=None):
    """Solve linear assignment problem.

    Parameters
    ----------
    distance_metric : Callable[List[Track], List[Detection], List[int], List[int]) -> ndarray
        The distance metric is given a list of tracks and detections as well as
        a list of N track indices and M detection indices. The metric should
        return the NxM dimensional cost matrix, where element (i, j) is the
        association cost between the i-th track in the given track indices and
        the j-th detection in the given detection_indices.
    max_distance : float
        Gating threshold. Associations with cost larger than this value are
        disregarded.
    tracks : List[track.Track]
        A list of predicted tracks at the current time step.
    detections : List[detection.Detection]
        A list of detections at the current time step.
    track_indices : List[int]
        List of track indices that maps rows in `cost_matrix` to tracks in
        `tracks` (see description above).
    detection_indices : List[int]
        List of detection indices that maps columns in `cost_matrix` to
        detections in `detections` (see description above).

    Returns
    -------
    (List[(int, int)], List[int], List[int])
        Returns a tuple with the following three entries:
        * A list of matched track and detection indices.
        * A list of unmatched track indices.
        * A list of unmatched detection indices.

    """

    if track_indices is None:
        track_indices = np.arange(len(tracks))
    if detection_indices is None:
        detection_indices = np.arange(len(detections))

    if len(detection_indices) == 0 or len(track_indices) == 0:
        return [], track_indices, detection_indices  # Nothing to match.

    cost_matrix = distance_metric(tracks, detections, track_indices, detection_indices)
    cost_matrix[cost_matrix > max_distance] = max_distance + 1e-5
    indices = linear_assignment(cost_matrix)
    indices = np.hstack(
        [indices[0].reshape(((indices[0].shape[0]), 1)), indices[1].reshape(((indices[0].shape[0]), 1))]
    )

    matches, unmatched_tracks, unmatched_detections = [], [], []
    for col, detection_idx in enumerate(detection_indices):
        if col not in indices[:, 1]:
            unmatched_detections.append(detection_idx)
    for row, track_idx in enumerate(track_indices):
        if row not in indices[:, 0]:
            unmatched_tracks.append(track_idx)
    for row, col in indices:
        track_idx = track_indices[row]
        detection_idx = detection_indices[col]
        if cost_matrix[row, col] > max_distance:
            unmatched_tracks.append(track_idx)
            unmatched_detections.append(detection_idx)
        else:
            matches.append((track_idx, detection_idx))
    return matches, unmatched_tracks, unmatched_detections


def matching_cascade(
    distance_metric, max_distance, cascade_depth, tracks, detections, track_indices=None, detection_indices=None
):
    """Run matching cascade. In case some detection didn't match with any tracks, compare them with with tracks that
       were previously there but not now (are occluded but not deleted)

    Parameters
    ----------
    distance_metric : Callable[List[Track], List[Detection], List[int], List[int]) -> ndarray
        The distance metric is given a list of tracks and detections as well as
        a list of N track indices and M detection indices. The metric should
        return the NxM dimensional cost matrix, where element (i, j) is the
        association cost between the i-th track in the given track indices and
        the j-th detection in the given detection indices.
    max_distance : float
        Gating threshold. Associations with cost larger than this value are
        disregarded.
    cascade_depth: int
        The cascade depth, should be se to the maximum track age.
    tracks : List[track.Track]
        A list of predicted tracks at the current time step.
    detections : List[detection.Detection]
        A list of detections at the current time step.
    track_indices : Optional[List[int]]
        List of track indices that maps rows in `cost_matrix` to tracks in
        `tracks` (see description above). Defaults to all tracks.
    detection_indices : Optional[List[int]]
        List of detection indices that maps columns in `cost_matrix` to
        detections in `detections` (see description above). Defaults to all
        detections.

    Returns
    -------
    (List[(int, int)], List[int], List[int])
        Returns a tuple with the following three entries:
        * A list of matched track and detection indices.
        * A list of unmatched track indices.
        * A list of unmatched detection indices.

    """
    if track_indices is None:
        track_indices = list(range(len(tracks)))
    if detection_indices is None:
        detection_indices = list(range(len(detections)))

    unmatched_detections = detection_indices
    matches = []
    for level in range(cascade_depth):
        if len(unmatched_detections) == 0:  # No detections left
            break

        track_indices_l = [k for k in track_indices if tracks[k].time_since_update == 1 + level]
        if len(track_indices_l) == 0:  # Nothing to match at this level
            continue

        matches_l, _, unmatched_detections = min_cost_matching(
            distance_metric, max_distance, tracks, detections, track_indices_l, unmatched_detections
        )
        matches += matches_l
    unmatched_tracks = list(set(track_indices) - set(k for k, _ in matches))
    return matches, unmatched_tracks, unmatched_detections


def min_cost_matching_v1(
    feature_dist_metric,
    dist_metric,
    max_feat_dist,
    max_distance,
    tracks,
    detections,
    mix_coef,  # k(distance_cost) + (1-k)(feature_cost)
    track_indices=None,
    detection_indices=None,
):
    """
    combine the visual feature and distance together to create one distance metric
    e.g. distance = k(feature) + (1-k)distance
    Parameters
    ----------
    feature_dist_metric : function callback
        function to create feature cost matrix
    dist_metric: function callback
        function to create distance cost matrix
    max_distance
        maximum acceptable distance(meter)
    max_feat_dist
        maximum acceptable cosine distance between features
    tracks : List[track.Track]
        A list of predicted tracks at the current time step.
    detections : List[detection.Detection]
        A list of detections at the current time step.
    mix_coef
        How much thr score of distance should be combined with visual features
    track_indices : Optional[List[int]]
        List of track indices that maps rows in `cost_matrix` to tracks in
        `tracks` (see description above). Defaults to all tracks.
    detection_indices : Optional[List[int]]
        List of detection indices that maps columns in `cost_matrix` to
        detections in `detections` (see description above). Defaults to all
        detections.

    Returns
    -------
    (List[(int, int)], List[int], List[int])
        Returns a tuple with the following three entries:
        * A list of matched track and detection indices.
        * A list of unmatched track indices.
        * A list of unmatched detection indices.

    """
    if track_indices is None:
        track_indices = np.arange(len(tracks))
    if detection_indices is None:
        detection_indices = np.arange(len(detections))

    if len(detection_indices) == 0 or len(track_indices) == 0:
        return [], track_indices, detection_indices  # Nothing to match.

    dist_cost_matrix = dist_metric(tracks, detections, track_indices, detection_indices)
    dist_cost_matrix[dist_cost_matrix > max_distance] = max_distance + 1e-5

    feat_cost_matrix = feature_dist_metric(tracks, detections, track_indices, detection_indices)
    feat_cost_matrix[feat_cost_matrix > max_feat_dist] = max_feat_dist + 1e-5

    cost_matrix = (mix_coef * dist_cost_matrix) + ((1 - mix_coef) * feat_cost_matrix)
    final_max_distance = (mix_coef * max_distance) + ((1 - mix_coef) * max_feat_dist)
    # print("final_max_distance: ", final_max_distance)
    # print("max_distance: ", max_distance)
    # print("max_feat_dist: ", max_feat_dist)
    # cost_matrix[cost_matrix > final_max_distance] = final_max_distance + 1e-5

    try:
        indices = linear_assignment(cost_matrix)
    except Exception as ex:
        print(ex)
        print("cost_matrix: ", cost_matrix)
    indices = np.hstack(
        [indices[0].reshape(((indices[0].shape[0]), 1)), indices[1].reshape(((indices[0].shape[0]), 1))]
    )

    matches, unmatched_tracks, unmatched_detections = [], [], []
    for col, detection_idx in enumerate(detection_indices):
        if col not in indices[:, 1]:
            unmatched_detections.append(detection_idx)
    for row, track_idx in enumerate(track_indices):
        if row not in indices[:, 0]:
            unmatched_tracks.append(track_idx)
    for row, col in indices:
        track_idx = track_indices[row]
        detection_idx = detection_indices[col]
        if cost_matrix[row, col] > final_max_distance:
            unmatched_tracks.append(track_idx)
            unmatched_detections.append(detection_idx)
        else:
            matches.append((track_idx, detection_idx))
    return matches, unmatched_tracks, unmatched_detections


def min_cost_matching_v2(
    feature_dist_metric,
    dist_metric,
    max_feat_dist,
    max_distance,
    tracks,
    detections,
    mix_coef,  # k(distance_cost) + (1-k)(feature_cost)
    track_indices=None,
    detection_indices=None,
):
    """
    combine the visual feature and distance together to create one distance metric
    e.g. distance = k(feature) + (1-k)distance
    Parameters
    ----------
    feature_dist_metric : function callback
        function to create feature cost matrix
    dist_metric: function callback
        function to create distance cost matrix
    max_distance
        maximum acceptable distance(meter)
    max_feat_dist
        maximum acceptable cosine distance between features
    tracks : List[track.Track]
        A list of predicted tracks at the current time step.
    detections : List[detection.Detection]
        A list of detections at the current time step.
    mix_coef
        How much thr score of distance should be combined with visual features
    track_indices : Optional[List[int]]
        List of track indices that maps rows in `cost_matrix` to tracks in
        `tracks` (see description above). Defaults to all tracks.
    detection_indices : Optional[List[int]]
        List of detection indices that maps columns in `cost_matrix` to
        detections in `detections` (see description above). Defaults to all
        detections.

    Returns
    -------
    (List[(int, int)], List[int], List[int])
        Returns a tuple with the following three entries:
        * A list of matched track and detection indices.
        * A list of unmatched track indices.
        * A list of unmatched detection indices.

    """
    if track_indices is None:
        track_indices = np.arange(len(tracks))
    if detection_indices is None:
        detection_indices = np.arange(len(detections))

    if len(detection_indices) == 0 or len(track_indices) == 0:
        return [], track_indices, detection_indices  # Nothing to match.

    dist_cost_matrix = dist_metric(tracks, detections, track_indices, detection_indices)
    dist_cost_matrix[dist_cost_matrix > max_distance] = max_distance + 1e-5

    feat_cost_matrix = feature_dist_metric(tracks, detections, track_indices, detection_indices)
    feat_cost_matrix[feat_cost_matrix > max_feat_dist] = max_feat_dist + 1e-5

    cost_matrix = (mix_coef * dist_cost_matrix) + ((1 - mix_coef) * feat_cost_matrix)
    final_max_distance = (mix_coef * max_distance) + ((1 - mix_coef) * max_feat_dist)
    # print("final_max_distance: ", final_max_distance)
    # print("max_distance: ", max_distance)
    # print("max_feat_dist: ", max_feat_dist)
    # cost_matrix[cost_matrix > final_max_distance] = final_max_distance + 1e-5

    try:
        indices = linear_assignment(cost_matrix)
    except Exception as ex:
        print(ex)
        print("cost_matrix: ", cost_matrix)
    indices = np.hstack(
        [indices[0].reshape(((indices[0].shape[0]), 1)), indices[1].reshape(((indices[0].shape[0]), 1))]
    )

    matches, unmatched_tracks, unmatched_detections = [], [], []
    for col, detection_idx in enumerate(detection_indices):
        if col not in indices[:, 1]:
            unmatched_detections.append(detection_idx)
    for row, track_idx in enumerate(track_indices):
        if row not in indices[:, 0]:
            unmatched_tracks.append(track_idx)
    for row, col in indices:
        track_idx = track_indices[row]
        detection_idx = detection_indices[col]
        if cost_matrix[row, col] > final_max_distance:
            unmatched_tracks.append(track_idx)
            unmatched_detections.append(detection_idx)
        else:
            matches.append((track_idx, detection_idx))
    return matches, unmatched_tracks, unmatched_detections
