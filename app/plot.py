from __future__ import annotations

import numpy as np

import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore, QtWidgets
from tracks import Track
from pyqtgraph import PlotWidget
import pyqtgraph.parametertree as ptree
import pyqtgraph.graphicsItems.ScatterPlotItem


class PlotIpm:
    def __init__(self, x_range, y_range):
        self.app = pg.mkQApp()
        self.plot_widget = pg.PlotWidget()
        data = {}
        self.pen = pg.mkPen("w")
        self.size = 10
        self.brush = pg.mkBrush("b")

        splitter = QtWidgets.QSplitter()
        splitter.addWidget(self.plot_widget)
        splitter.show()
        self.plot_widget.setXRange(*x_range)
        self.plot_widget.setYRange(*y_range)
        pg.exec()

    def draw(self, tracks: list[Track]):

        xyz_wcs = np.array([[*track.detection.to_xyz_wcs(), track.track_id] for track in tracks])
        if xyz_wcs.size != 0:
            xz_wcs = xyz_wcs[:, [0, 2, 3]]
            kwargs = {"x": xz_wcs[:, 0], "y": xz_wcs[:, 1], "pen": self.pen, "brush": self.brush, "size": self.size}
        else:
            kwargs = {"x": [], "y": [], "pen": self.pen, "brush": self.brush, "size": self.size}
        item = pg.ScatterPlotItem(**kwargs)
        item.opts["useCache"] = False
        self.plot_widget.clear()
        self.plot_widget.addItem(item)
        self.plot_widget.repaint()

    #
    # def start(self):
