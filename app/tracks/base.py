from enum import Enum
from abc import ABC, abstractmethod
from app.detections import Detection


class TrackState(Enum):
    """
    Enumeration type for the single target track state. Newly created tracks are
    classified as `tentative` until enough evidence has been collected. Then,
    the track state is changed to `confirmed`. Tracks that are no longer alive
    are classified as `deleted` to mark them for removal from the set of active
    tracks.

    """

    Tentative = 1
    Confirmed = 2
    Deleted = 3


class Track(ABC):
    detection: Detection
    track_id: int

    @abstractmethod
    def f_cv(self, x, dt):
        pass

    @abstractmethod
    def h_cv(self, x):
        pass

    @abstractmethod
    def prediction_xyzah_wcs(self):
        pass

    @abstractmethod
    def prediction_xyz_wcs(self):
        pass

    @abstractmethod
    def prediction_xyzah_ccs(self):
        pass

    @abstractmethod
    def prediction_xyz_ccs(self):
        pass

    @abstractmethod
    def prediction_tlbr_ccs(self):
        pass

    @abstractmethod
    def predict(self):
        pass

    @abstractmethod
    def update(self, detection: Detection):
        pass

    @abstractmethod
    def mark_missed(self):
        pass

    @abstractmethod
    def is_tentative(self):
        pass

    @abstractmethod
    def is_confirmed(self):
        pass

    @abstractmethod
    def is_deleted(self):
        pass
