import numpy as np
from filterpy.kalman import MerweScaledSigmaPoints
from filterpy.kalman import UnscentedKalmanFilter as UKF
from filterpy.common import Q_discrete_white_noise
from app.detections import RsDetection
from app.projections import RsProjection
from .base import Track, TrackState


class RsTrack(Track):
    """
    A single target track with state space `(x, y, z, a, h)` and associated
    velocities, where `(x, y, z)` is the center of the 3D object, `a` is the
    aspect ratio and `h` is the height.

    Parameters
    ----------
    obj_info : ndarray
        initial state vector of object [xc, yc, z, a, h] in ccs
    track_id : int
        A unique track identifier.
    n_init : int
        Number of consecutive detections before the track is confirmed. The
        track state is set to `Deleted` if a miss occurs within the first
        `n_init` frames.
    max_age : int
        The maximum number of consecutive misses before the track state is
        set to `Deleted`.
    feature : Optional[ndarray]
        Feature vector of the detection this track originates from. If not None,
        this feature is added to the `features` cache.

    Attributes
    ----------
    mean : ndarray
        Mean vector of the initial state distribution.
    covariance : ndarray
        Covariance matrix of the initial state distribution.
    track_id : int
        A unique track identifier.
    hits : int
        Total number of measurement updates.
    age : int
        Total number of frames since first occurance.
    time_since_update : int
        Total number of frames since last measurement update.
    state : TrackState
        The current track state.
    features : List[ndarray]
        A cache of features. On each measurement update, the associated feature
        vector is added to this list.

    """

    def __init__(self, detection: RsDetection, track_id_callback, n_init, max_age, projector: RsProjection):
        self.detection = detection
        self.projector = projector
        self.track_id_callback = track_id_callback
        self.track_id = -1
        self.hits = 1
        self.age = 1
        self.time_since_update = 0

        self.state = TrackState.Tentative
        self.features = []
        if detection.feature is not None:
            self.features.append(detection.feature)

        self._n_init = n_init
        self._max_age = max_age

        self.dt = 0.1
        self.sigmas = MerweScaledSigmaPoints(10, alpha=0.2, beta=2.0, kappa=-7.0)
        self.ukf = UKF(dim_x=10, dim_z=5, fx=self.f_cv, hx=self.h_cv, dt=self.dt, points=self.sigmas)

        xyz_wcs = detection.to_xyzah_wcs()
        self.ukf.x = np.array([xyz_wcs[0], 0, xyz_wcs[1], 0, xyz_wcs[2], 0, xyz_wcs[3], 0, xyz_wcs[4], 0])
        self.ukf.R = np.diag([1000, 1000, 0.09, 0.09, 1000])
        # ukf.R = ukf.R @ ukf.R
        self.ukf.Q[0:2, 0:2] = Q_discrete_white_noise(2, dt=self.dt, var=0.1)
        self.ukf.Q[2:4, 2:4] = Q_discrete_white_noise(2, dt=self.dt, var=0.1)
        self.ukf.Q[4:6, 4:6] = Q_discrete_white_noise(2, dt=self.dt, var=0.1)
        self.ukf.Q[6:8, 6:8] = Q_discrete_white_noise(2, dt=self.dt, var=0.1)
        self.ukf.Q[8:10, 8:10] = Q_discrete_white_noise(2, dt=self.dt, var=0.1)

        # self.ukf.P = np.diag(
        #     [10000 ** 2, 10 ** 2, 1000 ** 2, 10 ** 2, 1000 ** 2, 10 ** 2, 1000 ** 2, 10 ** 2, 1000 ** 2, 10 ** 2]
        # )

    def f_cv(self, x, dt):
        """state transition function for a
        constant velocity pedestrian

        Track (x, y, z, a, h)
        `a` is the aspect ratio and `h` is the height.
        """
        # [x vx y vy z vz a va h va]
        F = np.array(
            [
                [1, dt, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 1, dt, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 1, dt, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 1, dt, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 1, dt],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            ]
        )

        return F @ x

    def h_cv(self, x):
        xyz = self.projector.wcs2ccs(np.array([x[0], x[2], x[4]]))
        final = np.append(xyz, [x[6], x[8]])

        return final

    def prediction_xyzah_wcs(self):
        """Get current position in bounding box format `(top left x, top left y,
        width, height)`.

        Returns
        -------
        ndarray
            The bounding box.

        """
        ret = self.ukf.x[[0, 2, 4, 6, 8]].copy()
        return ret

    def prediction_xyz_wcs(self):
        ret = self.ukf.x[[0, 2, 4]].copy()
        return ret

    def prediction_xyzah_ccs(self):
        """Get current position in bounding box format `(min x, miny, max x,
        max y)`.

        Returns
        -------
        ndarray
            The bounding box.

        """
        ret = self.prediction_xyzah_wcs().copy()
        xyz_ccs = self.projector.wcs2ccs(ret[:3])
        ret[:3] = xyz_ccs
        return ret

    def prediction_xyz_ccs(self):
        ret = self.prediction_xyzah_wcs().copy()
        xyz_ccs = self.projector.wcs2ccs(ret[:3])
        return xyz_ccs

    def prediction_tlbr_ccs(self):
        """in order to visualize bbounding box"""
        xyzah_ccs = self.prediction_xyzah_ccs()
        w = xyzah_ccs[3] * xyzah_ccs[4]
        h = xyzah_ccs[4]
        tlbr = np.array([int(xyzah_ccs[0] - w / 2), int(xyzah_ccs[1] - h / 2), int(w), int(h)])
        tlbr[2:4] += tlbr[0:2]
        return tlbr

    def predict(self):
        """Propagate the state distribution to the current time step using a
        Kalman filter prediction step.

        Parameters
        ----------
        kf : kalman_filter.KalmanFilter
            The Kalman filter.

        """
        self.ukf.predict()
        self.age += 1
        self.time_since_update += 1

    def update(self, detection: RsDetection):
        """Perform Kalman filter measurement update step and update the feature
        cache.

        Parameters
        ----------
        kf : UKF
            The Unscented Kalman filter.
        detection : LidarDetection
            The associated detection.

        """
        det_ccs = detection.to_xyzah_ccs()
        self.ukf.update(np.array([det_ccs[0], det_ccs[1], det_ccs[2], det_ccs[3], det_ccs[4]]))
        self.features.append(detection.feature)

        self.hits += 1
        self.time_since_update = 0
        if self.state == TrackState.Tentative and self.hits >= self._n_init:
            self.state = TrackState.Confirmed
            self.track_id = self.track_id_callback()

    def mark_missed(self):
        """Mark this track as missed (no association at the current time step)."""
        if self.state == TrackState.Tentative:
            self.state = TrackState.Deleted
        elif self.time_since_update > self._max_age:
            self.state = TrackState.Deleted

    def is_tentative(self):
        """Returns True if this track is tentative (unconfirmed)."""
        return self.state == TrackState.Tentative

    def is_confirmed(self):
        """Returns True if this track is confirmed."""
        return self.state == TrackState.Confirmed

    def is_deleted(self):
        """Returns True if this track is dead and should be deleted."""
        return self.state == TrackState.Deleted
