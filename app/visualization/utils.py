import cv2
import numpy as np


def draw_bbox_id(img: np.ndarray, bbox: np.ndarray, track_id: int, color: tuple = (0, 255, 0)):
    cv2.rectangle(img, bbox[0:2], bbox[2:4], color=color, thickness=1)
    cv2.putText(img, str(track_id), (bbox[0], bbox[1] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    return img
