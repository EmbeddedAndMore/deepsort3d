import numpy as np


def reject_outliers(data, m=2.0):
    """Reject outliers from distance meaurement"""
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d / mdev if mdev else 0.0
    return data[s < m]


def get_depth(lidar_distances: np.ndarray, acceptable_tolerance: float, subsample: int) -> float:
    print("shape: ", lidar_distances.shape)
    sub_sample = np.random.choice(lidar_distances, int(len(lidar_distances) / subsample))
    cleaned = reject_outliers(sub_sample, acceptable_tolerance)
    if cleaned.size > 0:
        depth = cleaned.mean()
        return depth
    return None
