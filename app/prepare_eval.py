"""After running the tracker in eval mode now the generated data should be moved to specific loc to do the Evaluation"""
import glob
import shutil
import re

round = 1

src_base = "/home/eam/MasterThesis/Thesis/deep-sort3d/app"
source_paths = [
    # "track_result_keypoints",
    # "track_result_upper",
    # "track_result_upper_rect",
    "track_result_yolov5m_bbox",
    # "track_result_yolov5m_upper_rect",
]

dst_base = "/home/eam/MasterThesis/Thesis/TrackEval/data/trackers/kitti/kitti_2d_box_train"
dst_paths = [
    # f"keypoints_{round}",
    # f"upper_keypoint_{round}",
    # f"upper_rect_{round}",
    f"yolov5m_bbox{round}",
    # f"yolov5m_upper_rect_{round}",
]


for index, src_path in enumerate(source_paths):
    src_files = glob.glob(f"{src_base}/{src_path}/*.txt")
    for file in src_files:
        str_path = str(file)
        file_name = str_path.split("/")[-1]
        dst_file_name = re.sub("Track_", "", file_name)
        dst_full_path = f"{dst_base}/{dst_paths[index]}/data/{dst_file_name}"
        shutil.copy(file, dst_full_path)
        print("copy from: ", file, " to ", dst_full_path)
