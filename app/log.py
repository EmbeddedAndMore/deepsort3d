import cv2
import numpy as np
from typing import Tuple, List


def text_size(text: str, font: int, scale: float, thickness: int) -> Tuple[int, int]:
    """Determines width and height of a text based on following params

    :param text: text to be written
    :param font: font of the text
    :param scale: font scale
    :param thickness:  thickness of the font
    :return:  Tuple(width, height)
    """
    size = cv2.getTextSize(text, font, scale, thickness)
    return size[0][0], size[0][1] + size[1]


def overlay_log(img: np.ndarray, logs: List[dict], over_img=False) -> np.ndarray:
    """write croup of log on image or on the border

    It writes logs on image (over a transparent rectangle) or creates a border and
    writes the long on it. every dict will be written in a vertical block

    :param img: image on which we want to write a log
    :param logs: logs to be written
    :param over_img: if True write on image otherwise on the border
    :return:
    """
    block_offset = 10
    vertical_offset = 10
    img_overlaid = img.copy()
    font = cv2.FONT_HERSHEY_SIMPLEX
    font_scale = 0.5
    font_color = (255, 0, 0)
    thickness = 1
    x, y, w, h = 0, 0, 0, 0
    all_texts = []
    for section in logs:
        iw, ih = 0, 0
        sec_texts = {}
        for item in section.items():
            text = f"{item[0]}: {item[1]}"
            size = text_size(text, font, font_scale, thickness)
            iw = max(iw, size[0])
            ih += size[1]
            sec_texts[text] = (iw, ih)
        w += iw + block_offset
        h = max(h, ih)
        all_texts.append(sec_texts)
    h += vertical_offset

    if not over_img:
        img_overlaid = np.zeros((img.shape[0] + h, img.shape[1], img.shape[2]), dtype=np.uint8)
        img_overlaid[y + h:, x:] = img

    else:
        sub_img = img[y:y + h, x:x + w]
        white_rect = np.ones(sub_img.shape, dtype=np.uint8) * 255
        alpha = 0.5
        res = cv2.addWeighted(sub_img, alpha, white_rect, 1 - alpha, 1.0)
        img_overlaid[y:y + h, x:x + w] = res

    for i in range(len(all_texts)):
        w_off = i * (max([x[0] for x in list(all_texts[i].values())]) + block_offset)
        for item in all_texts[i].items():
            img_overlaid = cv2.putText(img_overlaid, item[0], (w_off, item[1][1]), font, font_scale, font_color,
                                       thickness)
    return img_overlaid


