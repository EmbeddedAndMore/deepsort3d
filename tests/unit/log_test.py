import cv2
from app.log import *


def test_text_size():
    assert text_size("banana", cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1) == (58, 17)

