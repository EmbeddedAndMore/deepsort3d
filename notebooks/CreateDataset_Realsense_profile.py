import glob
import json
import cv2
import numpy as np
from typing import List
import torch
import sys
from torchreid.utils import FeatureExtractor
from dataclasses import dataclass
from pathlib import Path
from zipfile import ZipFile
import shutil
from itertools import groupby
import matplotlib.pyplot as plt 
from tqdm import tqdm
from time import sleep
import math
import gc
from memory_profiler import profile

print(torch.cuda.is_available())
print(torch.__version__)

model_path_0 = '/home/eam/MasterThesis/ReId/deep-person-reid/osnet_x1_0_imagenet.pth'
model_path_1 = '/home/eam/MasterThesis/ReId/deep-person-reid/osnet_ain_x1_0_msmt17_256x128_amsgrad_ep50_lr0.0015_coslr_b64_fb10_softmax_labsmth_flip_jitter.pth'
model_path_2 = '/home/eam/MasterThesis/ReId/deep-person-reid/osnet_ain_x1_0_market1501_256x128_amsgrad_ep100_lr0.0015_coslr_b64_fb10_softmax_labsmth_flip_jitter.pth'
model_path_3 = '/home/eam/MasterThesis/ReId/deep-person-reid/osnet_ain_x1_0_dukemtmcreid_256x128_amsgrad_ep90_lr0.0015_coslr_b64_fb10_softmax_labsmth_flip_jitter.pth'
extractor = FeatureExtractor(
    model_name = 'osnet_ain_x1_0',
    model_path = model_path_2,
    device='cuda'
)

model = torch.hub.load('ultralytics/yolov5', 'yolov5x6', force_reload=False)  # or yolov5m, yolov5l, yolov5x, custom

@dataclass
class ObjectModel:
    bbox : List[int]
    upper : List[int]
    features : List[float]
    lidars: List[List[float]]

@dataclass
class ImageModel:
    img_name:str
    objects:List[ObjectModel]

def get_upper_body_rectangle(bbox: List[int], sub_bbox_ratio):
    """
    convert bbox yo in:
    [x, y, width, height]
    """
    sub_width = int(bbox[2] * sub_bbox_ratio)
    sub_height = int(bbox[3] * sub_bbox_ratio)
    return np.array([int(bbox[0]+(bbox[2]/2) - (sub_width/2)), int(bbox[1]+(bbox[3]/5)), sub_width, sub_height])

def crop(img, bboxes):
    cropped_imgs = []
    for bbox in bboxes:
        bbox = [x for x in bbox]
        cropped = img[bbox[1]:bbox[1] + bbox[3], bbox[0]:bbox[0] + bbox[2]]
        cropped_imgs.append(cropped)
    return cropped_imgs

def xyxy2xywh(xyxy):
    bbox = xyxy.clone().detach()
    bbox[2:4] = bbox[2:4]-bbox[0:2]
    return bbox


@profile
def main():
    sub_bbox_ratio = 1/4

    # load the images path corresponding to the sub_dataset
    imgs = glob.glob(f"/home/eam/MasterThesis/Thesis/deep-sort3d/notebooks/extracted_rs_6_person/frames/*.jpg")
    imgs = sorted(imgs)

    depth_infos = glob.glob(f"/home/eam/MasterThesis/Thesis/deep-sort3d/notebooks/extracted_rs_6_person/depth/*.npy")
    depth_infos = sorted(depth_infos)





    images_info_upper_bbox = []
    images_info_bbox = []
    for i, img in enumerate(tqdm(imgs[:20])):
        img_name = str(img).split("/")[-1]
        img_path = str(img)#[x for x in imgs if img_name in x][0]
        frame = cv2.imread(img_path)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        
        depth_mat = np.load(depth_infos[i])
    #     print(img_name, depth_infos[i].split("/")[-1])

    #         print(img_name)
    #         print(img_path)

        height, width, channel = frame.shape

        result = model(frame, size=1280)
        # extract humans with satisfied threshold
        detection = [det.to(torch.int) for det in result.pred[0] if det[5] == 0.0 and det[4] > 0.55] 

        objects_info_upper_bbox = []
        objects_info_bbox = []
        for obj in detection:
            bbox = xyxy2xywh(obj[0:4])

            upper_body = get_upper_body_rectangle(bbox, sub_bbox_ratio)
    #                     upper_body = [int(round(x)) for x in upper_points2bbox(upper_body)]
            cropped_obj = crop(frame, [bbox])[0]
            cropped_obj = cv2.cvtColor(cropped_obj, cv2.COLOR_BGR2RGB)
            features = extractor(cropped_obj)
            #lidars = project_to_upper_bodies(imgfov_pc_pixel, imgfov_pc_cam2, upper_body)
            depth_cropped = crop(depth_mat, [upper_body])[0]
            if depth_cropped.size == 0:
                continue
            np_features = features.squeeze().cpu().detach().numpy().astype(float)
            obj_model = ObjectModel(bbox.tolist(), upper_body.tolist(), np.round(np_features,6).tolist(), np.round(depth_cropped,2).tolist())
            objects_info_upper_bbox.append(obj_model.__dict__)


            bbox_body = bbox.cpu()
    #         lidars = project_to_upper_bodies(imgfov_pc_pixel, imgfov_pc_cam2, bbox_body)
            depth_cropped = crop(depth_mat, [bbox_body])[0]
            if depth_cropped.size == 0:
                continue
            obj_model_bbox = ObjectModel(bbox.tolist(), bbox_body.tolist(), np.round(np_features,6).tolist(), np.round(depth_cropped,2).tolist())
            objects_info_bbox.append(obj_model_bbox.__dict__)

        img_model_upper_bbox = ImageModel(img_name, objects_info_upper_bbox)
        images_info_upper_bbox.append(img_model_upper_bbox.__dict__)

        img_model_bbox = ImageModel(img_name, objects_info_bbox)
        images_info_bbox.append(img_model_bbox.__dict__)
        
        gc.collect()
    #     if i != 0 and i % 200 == 0:
    #         json_images_info = json.dumps(images_info_upper_bbox, indent=4,)
    #         with open(f"dataset_realsense_yolov5x6_upper_bbox_osnet_ain_x1_0_dukemtmcreid/dataset_final_realsense_{i}.json", "w") as f:
    #             f.write(json_images_info)

    #         json_images_info = json.dumps(images_info_bbox, indent=4,)
    #         with open(f"dataset_realsense_yolov5x6_bbox_osnet_ain_x1_0_dukemtmcreid/dataset_final_realsense_{i}.json", "w") as f:
    #             f.write(json_images_info)
            
    # #         del images_info_upper_bbox
    # #         images_info_upper_bbox = []
            
    # #         del images_info_bbox
    # #         images_info_bbox = []
            
    #         print("Cleaned")

    json_images_info = json.dumps(images_info_upper_bbox, indent=4,)
    with open(f"dataset_realsense_yolov5x6_upper_bbox_osnet_ain_x1_0_dukemtmcreid/dataset_final_realsense_{i}.json", "w") as f:
        f.write(json_images_info)

    json_images_info = json.dumps(images_info_bbox, indent=4,)
    with open(f"dataset_realsense_yolov5x6_bbox_osnet_ain_x1_0_dukemtmcreid/dataset_final_realsense_{i}.json", "w") as f:
        f.write(json_images_info)

if __name__ == "__main__":
    main()